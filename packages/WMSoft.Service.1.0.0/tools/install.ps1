param($installPath, $toolsPath, $package, $project)

function Search-ItemsRecursive($projectItems, $list){
  ForEach ($item in $projectItems) {
    #$itemName = $item.Name
    #Write-Host "Getting all project items for $itemName"
    
    if($item.ProjectItems -ne $null){
      Search-ItemsRecursive -projectItems $item.ProjectItems -list $list
    }
    else{
      [void]$list.Add($item)
    }
  }
}

function Replace-ItemsValue($item, $projectName){
  $filePath = $item.FileNames(1);
  # file is exist
  if(!(Test-Path $filePath)){
    return
  }
  # file is match xxx.dll
  if($filePath -match 'dll$'){
    return
  }
    
  try{
    $file = Get-Content $filePath
    #Write-Host $file
  
    $file = $file.Replace("[ProjectNamespace]", $projectName) #namespace
    $file = $file.Replace("[ServiceName]", $projectName)      #servicename
    Set-Content $filePath -Value $file -encoding utf8
  }
  catch
  {}
}


#$project.Save()
#Write-Host $project.FullName 

$projectName = $project.Name
Write-Host "ProjectName: $projectName"

# get all projectitems
$items = New-Object System.Collections.ArrayList($null)
Search-ItemsRecursive -projectItems $project.ProjectItems -list $items

# change placeholder
ForEach ($item in $items) { 
  Replace-ItemsValue -item $item -projectName $projectName
}

$project.Save()