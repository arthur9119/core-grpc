FROM microsoft/dotnet:2.0-runtime AS base
WORKDIR /app

FROM microsoft/dotnet:2.0-sdk AS build
WORKDIR /src
COPY *.sln ./
COPY src/Sodao.GrpcExample.Service/Sodao.GrpcExample.Service.csproj Sodao.GrpcExample.Service/
COPY src/Sodao.GrpcExample.Service.Grpc/Sodao.GrpcExample.Service.Grpc.csproj Sodao.GrpcExample.Service.Grpc/
COPY src/Sodao.Core.Grpc/Sodao.Core.Grpc.csproj Sodao.Core.Grpc/
RUN dotnet restore -s http://10.0.60.89:8081/nuget/ -s https://api.nuget.org/v3/index.json

COPY ./src/Sodao.GrpcExample.Service ./Sodao.GrpcExample.Service
COPY ./src/Sodao.GrpcExample.Service.Grpc/ ./Sodao.GrpcExample.Service.Grpc
COPY ./src/Sodao.Core.Grpc ./Sodao.Core.Grpc

WORKDIR /src/Sodao.GrpcExample.Service
RUN dotnet build Sodao.GrpcExample.Service.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish Sodao.GrpcExample.Service.csproj -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Sodao.GrpcExample.Service.dll"]
